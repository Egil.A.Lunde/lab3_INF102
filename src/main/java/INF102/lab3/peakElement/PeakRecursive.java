package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        if(numbers.size() == 1){
            return numbers.get(0);
    }
        List<Integer> bottomHalf = numbers.subList(0,numbers.size()/2);
        int bottom = peakElement(bottomHalf);
        
        List<Integer> topHalf = numbers.subList(numbers.size()/2,numbers.size());
        int top = peakElement(topHalf);
    
        return top>bottom?top:bottom;

    }

}
