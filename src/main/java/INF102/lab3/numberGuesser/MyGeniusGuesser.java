package INF102.lab3.numberGuesser;

public class MyGeniusGuesser implements IGuesser {

    private int lowerbound;
    private int upperbound;
	@Override
    public int findNumber(RandomNumber number) {
        lowerbound = number.getLowerbound();
        upperbound = number.getUpperbound();

        return makeGuess(number, lowerbound, upperbound);
    }
    
    public int makeGuess(RandomNumber number, int lowerbound, int upperbound){
        int guessNumber = (upperbound + lowerbound)/2;
        System.out.println(guessNumber);
        int guessResult = number.guess(guessNumber);

        if(guessResult == 0){
            return guessNumber;
        }

        else if(guessResult == -1){
            return makeGuess(number, guessNumber, upperbound);
        }

        else if(guessResult == 1){
            return makeGuess(number, lowerbound, guessNumber);
        }
        throw new IllegalArgumentException("Something cool happened :)");

    }
}